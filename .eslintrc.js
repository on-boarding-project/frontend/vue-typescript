module.exports = {
  root: false,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'prettier/prettier': 'error',
    'linebreak-style': 0,
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-shadow': 'off',
    'no-continue': 'off',
    'import/extensions': 'off',
    'import/prefer-default-export': 'off',
    'no-restricted-properties': [
      2,
      {
        object: 'disallowedObjectName',
        property: 'disallowedPropertyName',
      },
    ],
    quotes: [2, 'single', { avoidEscape: true }],
    'max-len': ['error', { code: 200 }],
    '@typescript-eslint/no-explicit-any': 'off',
    'no-param-reassign': 0,
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
};
